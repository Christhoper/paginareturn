import Vue from 'vue'
import App from './App.vue'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
import router from './router'
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false

const inputs = document.querySelectorAll(".input");

function addcl() {
  let parent = this.parentNode.parentNode;
  parent.classList.add("focus");
}

function remcl() {
  let parent = this.parentNode.parentNode;
  if (this.value == "") {
    parent.classList.remove("focus");
  }
}

inputs.forEach(input => {
  input.addEventListener("focus", addcl);
  input.addEventListener("blur", remcl);
});

let externalScript = document.createElement("script");
externalScript.setAttribute(
  "src",
  "https://kit.fontawesome.com/a81368914c.js"
);
document.head.appendChild(externalScript);

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
