import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import NavBar from '../components/NavBar.vue'

Vue.use(VueRouter)

Vue.directive('title', {
  inserted: (el, binding) => document.title = binding.value,
  update: (el, binding) => document.title = binding.value
})

const routes = [
  {
    path: '/home',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path: '/navBar',
    name: 'NavBar',
    component: NavBar
  },
]

const router = new VueRouter({
  routes
})

export default router
